# Semma Menu Scraper

This project aims to generate light-weight HTML websites from Semma restaurants' JSON APIs

By default this generates sites for daily and weekly menus seperately for Rentukka, Lozzi, Piato and Maija.

### Usage

Read the scripts and modify if desired.

### Dependencies

bash, coreutils, curl, jq, sponge
