#!/bin/bash

set -eu

# {{{ Open the HTML document
start() {
	cat <<-EOF
	<!DOCTYPE html>
	<meta charset="UTF-8">
	<html style="color:#e8e6e3; background-color: #181a1b;">
	<body>
	  <div class="wholepage">
	EOF

	STATE=start
}
# }}}

# {{{ Name of the restaurant
name() {
	# Variables, given or use these as default
	local NAME="${NAME-Ravintola Rentukka}"

	[ "$STATE" != "start" ] && \
		echo "    </div>" # </restaurant>

	echo "    <div class=\"restaurant\">"
	echo "      <p>"
	echo "        <hr>"
	echo "          <h2>$NAME</h2>"
	echo "        <hr>"
	echo "      </p>"

	STATE=name
}
# }}}

# {{{ Generate the main content of the HTML document
body() {
	# {{{ Variables, given or use these as default
	local API="${API-https://www.semma.fi/menuapi/day-menus}"
	local ID="${ID-?costCenter=1416}"
	local DATE="${DATE-"+0 day"}"
	local TIME="${TIME-&date=$(date --date="$DATE" --iso-8601=minutes)}"
	local LOCALE="${LOCALE-&language=${LANGUAGE-fi}}" # fi / en are at least supported
	local URL="${URL-$API$ID${TIME/+/%2B}${LOCALE#_*}}"
	# }}}

	# Be Nice :)
	sleep "${SLEEPDELAY-69}"
	JSON="$(curl "$URL")"

	# Check if name() was skipped
	[ "$STATE" = "start" ] && \
		echo "    <div class=\"restaurant\">"

	# {{{ Week day divider
	if [ "$DATE" != "+0 day" ]
	then
		echo "      <hr>"
	fi
	# }}}

	# {{{ Week day and date
	DAYOFWEEK="$(jq -r '.dayOfWeek' <<< "$JSON")"

	echo "      <div class=\"day\">"
	echo "        <p>"
	echo "          <h3>$DAYOFWEEK $(date --date="$DATE" --iso-8601=date)</h3>"
	echo "        </p>"
	# }}}

	# {{{ Diet sections
	SECTIONCOUNT="$(jq -r '.menuPackages | length' <<< "$JSON")"

	SECTIONS=()
	LOAD="$(jq -r '.menuPackages[] | .name + " " + .price' <<< "$JSON")"
	while read -r line
	do
		SECTIONS+=("$line")
	done <<< "$LOAD"
	# }}}

	# {{{ Menu
	for (( i = 0; i < "$SECTIONCOUNT"; ++i))
	do
		# How many dishes
		DISHCOUNT="$(jq -r '.menuPackages['"$i"'].meals | length' <<< "$JSON")"

		# Null entry? ok... skip!
		[ "$DISHCOUNT" -eq 0 ] && continue

		# {{{ Dish names and their dietary information
		DISHES=()
		LOAD="$(jq -r '.["menuPackages"]['"$i"']["meals"][] | .name + " " + (.diets | join(", "))' <<< "$JSON")"
		while read -r line
		do
			DISHES+=("$line")
		done <<< "$LOAD"
		# }}}

		# {{{ Diet section and its Dishes
		echo "        <div class=\"dish\">"
		echo "          <h4 style=\"margin-bottom:0;\">${SECTIONS[$i]}</h4>"
		echo "          <p class=\"details\" style=\"margin-top:0;\">"
		for (( j = 0; j < "$DISHCOUNT"; ++j))
		do
			echo "            ${DISHES[$j]}<br>"
		done
		echo "          </p>"
		echo "        </div>" # </dish>
		# }}}
	done
	echo "      </div>" # </day>

	STATE=body
}
# }}}

# {{{ Close the HTML document
end() {
	 echo "    </div>" # </restaurant>

	cat <<-EOF
	  </div>
	</body>
	</html>
	EOF

	STATE=end
}
# }}}
