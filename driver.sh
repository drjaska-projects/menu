#!/bin/bash

set -eu

# {{{ Path to this script
menudir="$(realpath "$(dirname "$0")")"
cd "$menudir"
# }}}

# {{{ Initialise sites in filesystem
if ! [ -s sites/daily/index.html ]
then
	mkdir -p sites/daily/
	touch sites/daily/index.html
fi
if ! [ -s sites/weekly/index.html ]
then
	mkdir -p sites/weekly/
	touch sites/weekly/index.html
fi
if ! [ -s sites/rentukka/index.html ]
then
	mkdir -p sites/rentukka/
	touch sites/rentukka/index.html
fi
# }}}

# {{{ Import start, name, body, and end functions
source content-generator-lib.sh
# }}}

# {{{ Generate Rentukka site
{
	start

	#name
	body

	end
} | sponge sites/rentukka/index.html
# }}}

# {{{ Generate Daily site
{
	start

	NAME="Ravintola Rentukka" name
	ID="?costCenter=1416" body

	NAME="Ravintola Lozzi" name
	ID="?costCenter=1401" body

	NAME="Ravintola Piato" name
	ID="?costCenter=1408" body

	NAME="Ravintola Maija" name
	ID="?costCenter=1402" body

	end
} | sponge sites/daily/index.html
# }}}

# {{{ Generate Weekly site
{
	start

	NAME="Ravintola Rentukka" name
	for i in $(seq 0 "${FUTUREDAYS-6}")
	do
		DATE="+$i day" \
		ID="?costCenter=1416" \
			body
	done

	NAME="Ravintola Lozzi" name
	for i in $(seq 0 "${FUTUREDAYS-6}")
	do
		DATE="+$i day" \
		ID="?costCenter=1401" \
			body
	done

	NAME="Ravintola Piato" name
	for i in $(seq 0 "${FUTUREDAYS-6}")
	do
		DATE="+$i day" \
		ID="?costCenter=1408" \
			body
	done

	NAME="Ravintola Maija" name
	for i in $(seq 0 "${FUTUREDAYS-6}")
	do
		DATE="+$i day" \
		ID="?costCenter=1402" \
			body
	done

	end
} | sponge sites/weekly/index.html
# }}}
